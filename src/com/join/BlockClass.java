package com.join;

import java.util.Objects;


public class BlockClass implements Block {
    private final String color;
    private final String material;

    public BlockClass(String color, String material) {
        this.color = color;
        this.material = material;
    }

    @Override
    public String getColor() {
        return color;
    }

    @Override
    public String getMaterial() {
        return material;
    }


    @Override
    public String toString() {
        return "BlockClass{" +
                "color='" + color + '\'' +
                ", material='" + material + '\'' +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BlockClass block = (BlockClass) o;
        return Objects.equals(color, block.color) &&
                Objects.equals(material, block.material);
    }

    @Override
    public int hashCode() {
        return Objects.hash(color, material);
    }
}