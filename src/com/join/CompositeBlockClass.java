package com.join;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;


public class CompositeBlockClass extends BlockClass implements CompositeBlock {

    private List<Block> blocks = new LinkedList<>();

    public CompositeBlockClass(String color, String material) {
        super(color, material);
    }

    @Override
    public List<Block> getBlocks() {
        return Collections.unmodifiableList(blocks);
    }

    public void addBlock(Block block) {
        blocks.add(block);
    }



    @Override
    public String toString() {
        return "CompositeBlockClass{" +
                "blocks=" + blocks +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        CompositeBlockClass that = (CompositeBlockClass) o;
        return Objects.equals(blocks, that.blocks);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), blocks);
    }


}