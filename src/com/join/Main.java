package com.join;

import java.util.List;
import java.util.Optional;

public class Main {

    public static void main(String[] args) {
        BlockClass blo1 = new BlockClass("czerwony","drewno");
        BlockClass blo2 = new BlockClass("biały","plastik");
        BlockClass blo3 = new BlockClass("pomarańczowy","drewno");
        BlockClass blo4 = new BlockClass("pomarańczowy","plastik");
        BlockClass blo5 = new BlockClass("niebieski","metal");
        BlockClass blo6 = new BlockClass("beżowy","metal");

        CompositeBlockClass com1= new CompositeBlockClass("brązowy","plastik");
        com1.addBlock(blo1);
        com1.addBlock(blo2);
        com1.addBlock(blo3);

        CompositeBlockClass com2= new CompositeBlockClass("czarny","plastik");
        com2.addBlock(blo4);
        com2.addBlock(blo1);
        com2.addBlock(blo1);

        CompositeBlockClass com3= new CompositeBlockClass("kremowy","metal");
        com3.addBlock(blo5);
        com3.addBlock(blo4);
        com3.addBlock(blo3);

        CompositeBlockClass com4= new CompositeBlockClass("czarny","metal");
        com4.addBlock(com3);

        CompositeBlockClass comX= new CompositeBlockClass("purpurowy","stal");
        comX.addBlock(com2);
        comX.addBlock(com4);
        comX.addBlock(com1);

        CompositeBlockClass com5= new CompositeBlockClass("niebieski","plastik");
        comX.addBlock(com5);

        Wall wall= new Wall(comX,blo6);


        List<Block> listmaterial = wall.findBlocksByMaterial("plastik");

        Optional<Block> optionalcolor= wall.findBlockByColor("purpurowy");
        Block blockcolor= optionalcolor.orElse(null);
        System.out.println("count1  "+wall.count());
        System.out.println("findCLOR1  "+blockcolor);
        System.out.println("findMATERIAL1  "+listmaterial);

        wall.addBlock(com1);

        List<Block> listmaterial2 = wall.findBlocksByMaterial("plastik");
        System.out.println("count2 "+wall.count());
        System.out.println("findMATERIAL2  "+listmaterial2);

    }
}
