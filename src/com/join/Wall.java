package com.join;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

interface Structure {
    // zwraca dowolny element o podanym kolorze
    Optional<Block> findBlockByColor(String code);
    // zwraca wszystkie elementy z danego materiału
    List<Block> findBlocksByMaterial(String renderer);
    //zwraca liczbę wszystkich elementów tworzących strukturę
    int count();
}
public class Wall implements Structure {
    private final List<Block> blocks = new LinkedList<>();



    public Wall(Block... blocks) {
        this.blocks.addAll(List.of(blocks));
    }

    /**
     * Metoda wyszukująca dany kolor w strukturze
     * @poram String color, szukany kolor
     * @return znaleziony przez metodę typ Optional<Block>.
     */
    public Optional<Block> findBlockByColor(String color) {
        if (color == null) {
            throw new IllegalArgumentException("Color is null!");
        }
        Optional<Block> optionalValue =toStream().filter(n -> color.equals(n.getColor())).findFirst();
        return optionalValue;
    }

    /**
     * Metoda wyszukująca dany materiał w strukturze
     * @poram String material, szukany materiał
     * @return Lista znalezionych elementów struktury wykonanych z danego metariału.
     */
    public List<Block> findBlocksByMaterial(String material) {
        if (material == null) {
            throw new IllegalArgumentException("material is null!");
        }
        List<Block> result = toStream().filter(n -> material.equals(n.getMaterial())).collect(Collectors.toList());
        return result;
    }

    /**
     * Metoda obliczająca liczbę elementów danej struktury
     * @return liczba elementów tworzących strukturę.
     */
    public int count() {
        return (int) toStream().count();
    }

    /**
     * Metoda dodająca element do struktury
     * @poram Block block, Obiekt Block dodawany do struktury
     */
    public void addBlock(Block block) {
        blocks.add(block);
    }

    /**
     * Metoda zamieniajaca strukturę w strumień
     * @return utworzony strumień.
     */
    public Stream<Block> toStream() {

        int index=0;

        List<Block> expandedblocks = new LinkedList<>();
        expandedblocks.addAll(blocks);

        while(expandedblocks.size()>index){
            Block block=expandedblocks.get(index);

            if(block instanceof CompositeBlock ){

                expandedblocks.addAll(expandedblocks.size() ,((CompositeBlock) block).getBlocks());
            }

            index++;
        }
        return expandedblocks.stream();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Wall that = (Wall) o;
        return Objects.equals(blocks, that.blocks);
    }

    @Override
    public int hashCode() {
        return Objects.hash(blocks);
    }
}

interface Block {
    String getColor();
    String getMaterial();

}
interface CompositeBlock extends Block {
    List<Block> getBlocks();
}